# Orca AI 🐋

Welcome to Orca, your new AI assistant powered by cutting-edge technology. Unlike other AI assistants, Orca is designed to be locally hosted, ensuring the highest level of privacy and security. With Orca, you can enjoy the convenience of an AI assistant without compromising your personal data. Not only that, but Orca has the ability to access the internet, providing you with real-time information and assistance whenever you need it. Best of all, Orca is free and open-source software (FOSS), meaning you have full control over its code and can customize it to suit your needs. Say hello to a smarter and more independent way of managing your tasks with Orca!

Additionally, Orca has a trick up its sleeve: **integration**.
This means that Orca can inject her beauty into a number of our programs.
Docs? Bam, write you a paragraph.
Blade? Bam, remove background.
Qode? Here's the universe rewritten in Rust.

It's free, it's strong, it's simply Orca.